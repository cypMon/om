/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.content.Context
import android.graphics.Canvas
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.text.style.LineBackgroundSpan

/**
@author Miroslav Mazel
 */
class CenteredBackgroundSpan(context: Context) : LineBackgroundSpan {
    private val linePaint = Paint()
    private val dashLength = context.resources.getDimension(R.dimen.dash_length)
    private val yOffset = context.resources.getDimension(R.dimen.underline_y_offset)
    private var cached = false
    private var xOffset = 0f
    private var length = 0f

    init {
        linePaint.strokeWidth = 3f
        linePaint.pathEffect = DashPathEffect(floatArrayOf(dashLength, dashLength), 0f)
        linePaint.style = Paint.Style.FILL_AND_STROKE
    }

    //todo check how often this is called, optimize
    override fun drawBackground(c: Canvas, p: Paint, left: Int, right: Int, top: Int, baseline: Int, bottom: Int, text: CharSequence, start: Int, end: Int, lnum: Int) {
        linePaint.color = p.color

        if (!cached) {
            length = p.measureText(text.subSequence(start, end).toString())
            xOffset = (right.toFloat() + left) / 2 - length / 2
        }

        val path = Path()
        path.moveTo(xOffset, baseline + yOffset)
        path.lineTo(xOffset + length, baseline + yOffset)
        c.drawPath(path, linePaint)
    }
}