/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.app.NotificationCompat

/**
@author Miroslav Mazel
 */
class AudioService : Service() { //todo extract service lifecycle things into its own object

    private val meditationRetriever = MeditationRetriever()
    private var meditationType = MeditationType.GUIDED
    private var meditation = retrieveMeditation()
        set (value) {
            field = value
            presentMeditation()
            saveState()
        }

    private val notificationID = 1

    private var mediaPlayer = MediaPlayer()
    private var isPrepared = false
    private var asyncPosition = 0

    private var playState = PlayState.BLANK
        set (value) {
            field = value
            presentPlayState()
            saveState()
        }
    private var presenter: AudioContract.Presenter? = null
    private var connectionID: Int? = null

    private enum class PlayState {BLANK, STOPPED, PLAYING, PAUSED }

    private data class InternalState(val meditationItem: MeditationItem, val position: Int?,
                                     val meditationType: MeditationType) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readParcelable(MeditationItem::class.java.classLoader),
                parcel.readValue(Int::class.java.classLoader) as? Int,
                MeditationType.values()[parcel.readInt()])

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeParcelable(meditationItem, flags)
            parcel.writeValue(position)
            parcel.writeInt(meditationType.ordinal)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<InternalState> {
            override fun createFromParcel(parcel: Parcel): InternalState {
                return InternalState(parcel)
            }

            override fun newArray(size: Int): Array<InternalState?> {
                return arrayOfNulls(size)
            }
        }
    }

    private fun startConnection(newID: Int) {
        if (connectionID != null) stopSelf(connectionID!!)
        connectionID = newID
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        startConnection(startId)
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        stopForeground(true)
        return OmBinder()
    }

    override fun onRebind(intent: Intent?) {
        stopForeground(true)
        super.onRebind(intent)
    }

    private fun saveState() {
        presenter?.savePausedState(
                InternalState(
                        meditationItem = meditation,
                        position = if (playState == PlayState.PAUSED) mediaPlayer.currentPosition else null,
                        meditationType = meditationType
                ))
    }

    private fun presentPlayState() {
        when (playState) {
            PlayState.STOPPED -> presenter?.onAudioReset()
            PlayState.PAUSED -> presenter?.onAudioPaused()
            PlayState.PLAYING -> presenter?.onAudioPlayed()
        }
    }

    private fun presentMeditation() {
        presenter?.onMeditationSet(meditation = meditation, type = meditationType)
    }

    private fun retrieveMeditation(): MeditationItem {
        return meditationRetriever.getMeditation(meditationType)
    }

    private fun shouldRunInBackground(): Boolean {
        return playState == PlayState.PLAYING && presenter != null
    }

    private fun asyncPlay() {
        playState = PlayState.PLAYING

        if (isPrepared)
            mediaPlayer.start()
    }

    private fun asyncPause() {
        playState = PlayState.PAUSED

        if (isPrepared)
            mediaPlayer.pause()
    }

    private fun asyncReset() {
        playState = PlayState.STOPPED

        if (isPrepared) {
            mediaPlayer.pause()
            mediaPlayer.seekTo(0)
        }
    }

    private fun buildNotification(): Notification {
        val pendingIntent = PendingIntent.getActivity(
                this, 0, Intent(this, MainActivity::class.java), PendingIntent.FLAG_IMMUTABLE
        )

        return NotificationCompat.Builder(this)
                .setContentTitle(getString(meditation.titleString))
                .setContentText(getString(R.string.notification_description))
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentIntent(pendingIntent)
                .build()
    }

    private fun changeMeditation() {
        println("changing")
        meditation = retrieveMeditation()

        resetMediaPlayer()
        setUpMediaPlayer()

        playState = PlayState.STOPPED
    }

    private fun resetMediaPlayer() {
        mediaPlayer.release()
        isPrepared = false
        asyncPosition = 0
        mediaPlayer = MediaPlayer()
    }

    private fun setUpMediaPlayer() {
        mediaPlayer.reset()

        val afd = resources.openRawResourceFd(meditation.mediaFileRaw) ?: return
        mediaPlayer.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
        afd.close()

        mediaPlayer.prepareAsync()
        mediaPlayer.setOnPreparedListener {
            isPrepared = true

            mediaPlayer.seekTo(asyncPosition)

            if (playState == PlayState.PLAYING)
                mediaPlayer.start()
        }

        mediaPlayer.setOnCompletionListener {
            playState = PlayState.STOPPED
            stopStartedService()
        }
    }

    private fun stopStartedService() {
        stopForeground(true)
        if (connectionID != null) stopSelf(connectionID!!)
        connectionID = null
    }

    override fun onUnbind(intent: Intent?): Boolean {
        if (shouldRunInBackground())
            startForeground(notificationID, buildNotification())
        else
            stopStartedService()

        presenter = null

        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }

    private fun restoreService(savedState: InternalState) {
        meditationType = savedState.meditationType
        meditation = savedState.meditationItem

        setUpMediaPlayer()

        val position = savedState.position
        if (position != null) {
            playState = PlayState.PAUSED

            asyncPosition = position
            if (isPrepared)
                mediaPlayer.seekTo(position)
        } else playState = PlayState.STOPPED
    }

    private fun restorePresenter() {
        presentMeditation()
        presentPlayState()
        saveState()
    }

    private fun createMeditation() {
        presentMeditation()
        playState = PlayState.STOPPED
        setUpMediaPlayer()
    }

    inner class OmBinder : Binder(), IBinder, AudioContract.External {
        override fun setPresenter(callback: AudioContract.Presenter, savedState: Parcelable?) {
            this@AudioService.presenter = callback

            if (playState == PlayState.BLANK) {
                if (savedState != null) {
                    println(":: Restoring state")
                    restoreService(savedState as InternalState)
                } else {
                    println(":: Starting")
                    createMeditation()
                }
            } else {
                println(":: Not blank")
                restorePresenter()
            }
        }

        override fun setMeditationType(meditationType: MeditationType) {
            if (this@AudioService.meditationType != meditationType) {
                this@AudioService.meditationType = meditationType
                this@AudioService.changeMeditation()
            }
        }

        override fun togglePlayPause() {
            if (this@AudioService.playState == PlayState.PLAYING) {
                this@AudioService.asyncPause()
            } else
                this@AudioService.asyncPlay()
        }

        override fun stop() {
            this@AudioService.asyncReset()
        }
    }
}