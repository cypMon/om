/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

/**
@author Miroslav Mazel
 */
class MeditationRetriever : IMeditationRetriever {
    override fun getMeditation(type: MeditationType): MeditationItem {
        return when (type) {
            MeditationType.GUIDED ->
                MeditationItem(
                        mediaFileRaw = R.raw.omorain_10min_breathing,
                        titleString = R.string.mdtn_title_omorain_10min_breathing,
                        author = MeditationAuthor(
                                titleId = R.string.mdtn_author_omorain,
                                linkId = R.string.mdtn_authorlnk_omorain,
                                imageId = R.drawable.emblem_author_omorain),
                        license = MeditationLicense(
                                titleId = R.string.license_cc_by_nc_sa_4,
                                linkId = R.string.licenselnk_cc_by_nc_sa,
                                imageId = R.drawable.emblem_creativecommons
                        ),
                        source = MeditationSource(
                                titleId = R.string.mdtn_src_freemindfulness,
                                linkId = R.string.mdtn_srclnk_freemindfulness,
                                imageId = R.drawable.emblem_freemindfulness
                        ),
                        changesString = null
                )
            MeditationType.UNGUIDED -> MeditationItem(
                    mediaFileRaw = R.raw.morgan_10min_unguided,
                    titleString = R.string.mdtn_title_morgan_10min_unguided,
                    author = MeditationAuthor(
                            titleId = R.string.mdtn_author_morgan,
                            linkId = R.string.mdtn_authorlnk_morgan,
                            imageId = null),
                    license = MeditationLicense(
                            titleId = R.string.license_cc_by_nc_sa_4,
                            linkId = R.string.licenselnk_cc_by_nc_sa,
                            imageId = R.drawable.emblem_creativecommons
                    ),
                    source = MeditationSource(
                            titleId = R.string.mdtn_src_freemindfulness,
                            linkId = R.string.mdtn_srclnk_freemindfulness,
                            imageId = R.drawable.emblem_freemindfulness
                    ),
                    changesString = null
            )
        }
    }
}