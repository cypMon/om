/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_meditation_detail.*


class MeditationDetailActivity : AppCompatActivity() { //todo bug: state not retained in backstack

    companion object {
        const val MEDITATION_EXTRA = "meditation"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meditation_detail)

        val meditation = intent.getParcelableExtra<MeditationItem>(MEDITATION_EXTRA)

        upButton.setOnClickListener {
            //todo perhaps figure out how to do proper up navigation
            NavUtils.navigateUpFromSameTask(this)
        }

        meditationTitle.text = getString(meditation.titleString)

        authorText.text = String.format(
                getString(R.string.author_text),
                getString(meditation.author.titleId))
        authorEmblem.setImageResource(meditation.author.imageId ?: R.drawable.emblem_author_generic)
        val authorLink = meditation.author.linkId
        if (authorLink != null) {
            authorContainer.setOnClickListener {
                val linkIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(authorLink)))
                startActivity(linkIntent)
            }
        }

        sourceText.text = String.format(
                getString(R.string.source_text),
                getString(meditation.source.titleId))
        val sourceImage = meditation.source.imageId
        if (sourceImage != null)
            sourceEmblem.setImageResource(sourceImage)
        val sourceLink = meditation.source.linkId
        if (sourceLink != null) {
            sourceContainer.setOnClickListener {
                val linkIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(sourceLink)))
                startActivity(linkIntent)
            }
        }

        licenseText.text = String.format(
                getString(R.string.license_text),
                getString(meditation.license.titleId))
        licenseEmblem.setImageResource(meditation.license.imageId)
        licenseContainer.setOnClickListener {
            val linkIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(meditation.license.linkId)))
            startActivity(linkIntent)
        }

        if (meditation.changesString != null)
            changesText.text = getString(meditation.changesString)
    }
}
